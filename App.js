import React, { useState } from "react";
import { createStore, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { Provider } from "react-redux";
import exercisesReducer from "./store/reducers/exercises";
import workoutProgramsReducer from "./store/reducers/workoutPrograms";
import activeWorkoutReducer from "./store/reducers/activeWorkout";
import completedWorkoutsReducer from "./store/reducers/completedWorkouts";

import * as Font from "expo-font";
import AppLoading from "expo-app-loading";

import WorkoutNavigator from "./navigation/WorkoutNavigator";

const rootReducer = combineReducers({
  exercises: exercisesReducer,
  workoutPrograms: workoutProgramsReducer,
  activeWorkout: activeWorkoutReducer,
  completedWorkouts: completedWorkoutsReducer,
});

const store = createStore(rootReducer, composeWithDevTools());

const fetchFonts = () => {
  return Font.loadAsync({
    "open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
    "open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf"),
  });
};

export default function App() {
  const [fontsLoaded, setfontsLoaded] = useState(false);

  if (!fontsLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setfontsLoaded(true)}
        onError={(error) => {
          console.log(error);
        }}
      />
    );
  }

  return (
    <Provider store={store}>
      <WorkoutNavigator />
    </Provider>
  );
}
