## Workout-app
First attempt at a React Native app, using Expo's managed workflow. Using React Native's stack & tab navigation, and Redux for state management.
All in memory data, no database yet. Created exclusively on a Sony Xperia XZ1 Compact, no platform/screen size specific styling just yet.

## Instructions

* Exercises can be filtered by category (muscle group filter isn't functional yet).
* Starting one of the two default workouts will show a small active workout component, when navigating to one of the parent screens.
* Sets can be swiped left to delete.
* A completed workout is shown in the history screen.

## Live demo

* [Demo on appetize](https://appetize.io/app/gf9ebnr4me7h4vdnkzc7hzr45r)

## To-do

* Adding charts and history information to the exercise details screen, based on completed workouts.
* Properly style and refactor history details screen, add onClick link to exercise details charts.
* Add ability to create and edit a workout.
* Add a check for empty sets when finishing a workout. Discard workout if no sets are filled out.
* Properly style the create/edit exercise screen.
* After adding an exercise, it doesn't show up immediately in the overview, still needs a reload.
* Implement functionality of filtering exercises by muscle group (should be multi-select).
* Possibility to navigate back to a different stack.
* General styling.
* Add styling for IOS and different android screen sizes.
* Setup API & database.
