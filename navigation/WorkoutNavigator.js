import React from "react";
import { createAppContainer } from "react-navigation";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import { createStackNavigator } from "react-navigation-stack";

import ExercisesScreen from "../screens/ExercisesScreen";
import ExerciseDetailsScreen from "../screens/ExerciseDetailsScreen";
import ExerciseEditScreen from "../screens/ExerciseEditScreen";
import WorkoutsScreen from "../screens/WorkoutsScreen";
import WorkoutDetailsScreen from "../screens/WorkoutDetailsScreen";
import WorkoutEditScreen from "../screens/WorkoutEditScreen";
import WorkoutStartScreen from "../screens/WorkoutStartScreen";
import HistoryScreen from "../screens/HistoryScreen";
import HistoryWorkoutDetailsScreen from "../screens/HistoryWorkoutDetailsScreen";

import Colors from "../constants/Colors";

import {
  Entypo,
  MaterialCommunityIcons,
  MaterialIcons,
} from "@expo/vector-icons";
import ActiveWorkoutScreen from "../screens/ActiveWorkoutScreen";

const defaultNavigationOptions = {
  headerStyle: {
    backgroundColor: Colors.primaryDark,
  },
  headerTitleStyle: {
    fontFamily: "open-sans-bold",
  },
  headerBackTitleStyle: {
    fontFamily: "open-sans-bold", //only ios has back title text
  },
  headerTintColor: Colors.accentLight,
};

const ExercisesNavigator = createStackNavigator(
  {
    Exercises: ExercisesScreen,
    ExerciseEdit: ExerciseEditScreen,
    ExerciseDetails: ExerciseDetailsScreen,
  },
  {
    defaultNavigationOptions: defaultNavigationOptions,
  }
);

const WorkoutsNavigator = createStackNavigator(
  {
    Workouts: WorkoutsScreen,
    WorkoutStart: WorkoutStartScreen,
    WorkoutEdit: WorkoutEditScreen,
    WorkoutDetails: WorkoutDetailsScreen,
    ActiveWorkout: ActiveWorkoutScreen,
  },
  {
    defaultNavigationOptions: defaultNavigationOptions,
  }
);

const HistoryNavigator = createStackNavigator(
  {
    History: HistoryScreen,
    HistoryWorkoutDetails: HistoryWorkoutDetailsScreen,
  },
  {
    defaultNavigationOptions: defaultNavigationOptions,
  }
);

const tabScreenConfig = {
  Exercises: {
    screen: ExercisesNavigator,
    navigationOptions: {
      tabBarIcon: (tabInfo) => {
        return (
          <MaterialIcons
            name={"fitness-center"}
            size={25}
            color={tabInfo.tintColor}
          />
        );
      },
      tabBarColor: Colors.primaryDark,
    },
  },
  Workouts: {
    screen: WorkoutsNavigator,
    navigationOptions: {
      tabBarIcon: (tabInfo) => {
        return (
          <MaterialCommunityIcons
            name={"weight-lifter"}
            size={25}
            color={tabInfo.tintColor}
          />
        );
      },
      tabBarColor: Colors.primaryDark,
    },
  },
  History: {
    screen: HistoryNavigator,
    navigationOptions: {
      tabBarIcon: (tabInfo) => {
        return <Entypo name={"archive"} size={25} color={tabInfo.tintColor} />;
      },
      tabBarColor: Colors.primaryDark,
    },
  },
};

const MainNavigator = createMaterialBottomTabNavigator(tabScreenConfig, {
  // labelStyle not available here for android
  activeColor: Colors.accentLight,
  shifting: true,
});

export default createAppContainer(MainNavigator);
