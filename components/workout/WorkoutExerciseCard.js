import React, { useState, useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Animated,
  TouchableNativeFeedback,
} from "react-native";
import Colors from "../../constants/Colors";
import Set from "../workout/Set";
import { FontAwesome } from "@expo/vector-icons";

import { SwipeListView } from "react-native-swipe-list-view";

const WorkoutExerciseCard = (props) => {
  const [Sets, setSets] = useState(
    props.sets.map((set) => {
      return { ...set, key: set.id };
    })
  );

  //console.log(Sets);
  const rowTranslateAnimatedValues = {};
  Sets.forEach((set) => {
    rowTranslateAnimatedValues[set.id] = new Animated.Value(1);
  });

  const animationIsRunning = useRef(false);

  const onSwipeValueChange = (swipeData) => {
    const { key, value } = swipeData;
    if (
      value < -Dimensions.get("window").width &&
      !animationIsRunning.current
    ) {
      animationIsRunning.current = true;

      Animated.timing(rowTranslateAnimatedValues[key], {
        toValue: 0,
        duration: 200,
        useNativeDriver: false,
      }).start(() => {
        const newData = [...Sets];
        const prevIndex = Sets.findIndex((item) => item.id === key);
        newData.splice(prevIndex, 1);

        newData.forEach((s, index) => {
          let numberofWarmups = newData.filter((s) => s.number === "W").length;

          if (s.number !== "W") {
            s.number = (index + 1 - numberofWarmups).toString();
          }
        });

        setSets(newData);
        animationIsRunning.current = false;
      });
    }
  };

  const renderItem = (data) => {
    let set = data.item;
    return (
      <Animated.View
        style={[
          styles.rowFrontContainer,
          {
            height: rowTranslateAnimatedValues[data.item.id].interpolate({
              inputRange: [0, 1],
              outputRange: [0, 60],
            }),
          },
        ]}
      >
        <Set
          isActiveWorkout={props.isActiveWorkout}
          id={set.id}
          workoutExerciseId={set.workoutExerciseId}
          key={set.id}
          number={set.number}
          weight={set.weight}
          reps={set.reps}
        ></Set>
      </Animated.View>
    );
  };

  const renderHiddenItem = () => (
    <View style={styles.rowBack}>
      <View style={[styles.backRightBtn, styles.backRightBtnRight]}>
        <FontAwesome name="trash" size={28} color="white" />
      </View>
    </View>
  );

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={styles.group}>
          <View>
            <Text style={styles.groupTitle}>{props.title}</Text>
            <View style={styles.setColumnHeaders}>
              <Text style={[styles.columnHeaders, { flexGrow: 15 }]}>#</Text>
              <Text style={[styles.columnHeaders, { flexGrow: 25 }]}>
                Previous
              </Text>
              <Text style={[styles.columnHeaders, { flexGrow: 25 }]}>
                (+KG)
              </Text>
              <Text style={[styles.columnHeaders, { flexGrow: 25 }]}>Reps</Text>
              <Text style={[styles.columnHeaders, { flexGrow: 10 }]}>
                <FontAwesome name="check-square-o" size={18} color="black" />
              </Text>
            </View>

            {/* {props.sets.map((set) => (
              <Set
                isActiveWorkout={props.isActiveWorkout}
                id={set.id}
                workoutExerciseId={set.workoutExerciseId}
                key={set.id}
                number={set.number}
                weight={set.weight}
                reps={set.reps}
              ></Set>
            ))} */}

            <SwipeListView
              data={Sets}
              renderItem={renderItem}
              disableRightSwipe
              renderHiddenItem={renderHiddenItem}
              rightOpenValue={-Dimensions.get("window").width}
              onSwipeValueChange={onSwipeValueChange}
              useNativeDriver={false}
            />

            <View style={styles.addSet}>
              <TouchableNativeFeedback
                onPress={() => {
                  let list = [...Sets];
                  let id = Math.floor(Math.random() * 10000).toString();
                  list.push({
                    key: id,
                    id: id,
                    number: (list.length + 1).toString(),
                    workoutExerciseId: props.weId,
                  });
                  setSets(list);
                }}
              >
                <Text style={styles.addSetText}>Add new set</Text>
              </TouchableNativeFeedback>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default WorkoutExerciseCard;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 16,
    // flexDirection: "column",
    backgroundColor: "#f7f7f7",
  },
  content: {
    //flex: 1,
  },
  groupTitle: {
    fontFamily: "open-sans",
    paddingLeft: 24,
    paddingVertical: 10,
    fontSize: 24,
    color: Colors.primaryLight,
  },
  setColumnHeaders: {
    flexDirection: "row",
    marginHorizontal: 10,
  },
  columnHeaders: {
    fontFamily: "open-sans-bold",
    fontSize: 14,
    paddingHorizontal: 10,
    color: "black",
    alignItems: "center",
  },
  group: {
    elevation: 1,
    marginVertical: 5,
    width: "100%",
    backgroundColor: "#fff",
  },
  addSet: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    paddingVertical: 10,
    backgroundColor: "white",
    height: 60,
  },
  addSetText: {
    fontFamily: "open-sans-bold",
    fontSize: 16,
    color: Colors.primaryLight,
  },

  rowFront: {
    alignItems: "center",
    backgroundColor: "#CCC",
    borderBottomColor: "black",
    borderBottomWidth: 1,
    justifyContent: "center",
    height: 50,
  },
  rowBack: {
    alignItems: "center",
    backgroundColor: "red",
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 15,
  },
  backRightBtn: {
    alignItems: "center",
    bottom: 0,
    justifyContent: "center",
    position: "absolute",
    top: 0,
    width: 75,
  },
  backRightBtnRight: {
    backgroundColor: "red",
    right: 0,
  },
});
