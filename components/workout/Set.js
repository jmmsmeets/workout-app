import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View, TextInput } from "react-native";
import Colors from "../../constants/Colors";
import Checkbox from "@react-native-form/checkbox";
import * as activeWorkoutsActions from "../../store/actions/activeWorkouts";
import { useDispatch } from "react-redux";

const Set = (props) => {
  const setId = props.id;
  const workoutExerciseId = props.workoutExerciseId;
  const number = props.number;

  const [Weight, setWeight] = useState(
    !props.isActiveWorkout ? props.weight : null
  );
  const [Reps, setReps] = useState(
    !props.isActiveWorkout ? props.weight : null
  );
  const dispatch = useDispatch();

  const onCompleteSetHandler = (value) => {
    if (value.checked) {
      let weight;
      let reps;

      if (!Weight) {
        weight = props.weight;
        setWeight(props.weight);
      } else {
        weight = Weight;
      }

      if (!Reps) {
        reps = props.reps;
        setReps(props.reps);
      } else {
        reps = Reps;
      }
      dispatch(
        activeWorkoutsActions.saveSet(
          setId,
          workoutExerciseId,
          number,
          weight,
          reps
        )
      );
    } else {
      if (Weight) {
        setWeight(null);
      }
      if (Reps) {
        setReps(null);
      }
      dispatch(activeWorkoutsActions.removeSet(setId, workoutExerciseId));
    }
  };

  return (
    <View style={styles.set}>
      <Text style={[styles.columnContent, { flexGrow: 15 }]}>
        {props.number}
      </Text>
      <Text style={[styles.columnContent, { flexGrow: 25 }]}>50kg x 8 </Text>
      <TextInput
        style={[styles.input, { flexGrow: 25 }]}
        placeholder={props.isActiveWorkout ? props.weight : null}
        value={Weight}
        onChangeText={(value) => setWeight(value)}
        keyboardType="number-pad"
      />
      <TextInput
        style={[styles.input, { flexGrow: 25 }]}
        placeholder={props.isActiveWorkout ? props.reps : null}
        value={Reps}
        onChangeText={(value) => setReps(value)}
        keyboardType="number-pad"
      />
      {props.isActiveWorkout ? (
        <Checkbox
          style={[styles.columnContent, { flexGrow: 10 }]}
          key={"completed"}
          label={""}
          positionLabel="right"
          marginTop={0}
          nativeComponent={false}
          color={Colors.primaryLight}
          disabled={false}
          checked={false}
          labelStyle={{}}
          containerStyle={{}}
          checkboxStyle={{}}
          switchStyle={{}}
          onValueChange={onCompleteSetHandler}
        />
      ) : (
        <View style={[styles.columnContent, { flexGrow: 10 }]}></View>
      )}
    </View>
  );
};

export default Set;

const styles = StyleSheet.create({
  set: {
    flex: 1,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "space-between",
    paddingHorizontal: 10,
    paddingVertical: 6,
    backgroundColor: "white",
  },
  columnContent: {
    alignSelf: "center",
  },
  input: {
    margin: 2,
    paddingVertical: 5,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    fontSize: 18,
  },
});
