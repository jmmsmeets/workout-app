import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text } from "react-native";
import Colors from "../../constants/Colors";

const timer = (countFrom) => {
  countFrom = new Date(countFrom).getTime();
  var now = new Date(),
    countFrom = new Date(countFrom),
    timeDifference = now - countFrom;

  var secondsInADay = 60 * 60 * 1000 * 24,
    secondsInAHour = 60 * 60 * 1000;

  days = Math.floor((timeDifference / secondsInADay) * 1);
  hours = Math.floor(((timeDifference % secondsInADay) / secondsInAHour) * 1);
  mins = Math.floor(
    (((timeDifference % secondsInADay) % secondsInAHour) / (60 * 1000)) * 1
  );
  secs = Math.floor(
    ((((timeDifference % secondsInADay) % secondsInAHour) % (60 * 1000)) /
      1000) *
      1
  );

  return (
    String(hours).padStart(2, "0") +
    ":" +
    String(mins).padStart(2, "0") +
    ":" +
    String(secs).padStart(2, "0")
  );
};

const ActiveWorkoutTimer = (props) => {
  const workoutStartDateTime = props.startDateTime;
  const [startDate] = useState(workoutStartDateTime);
  const [dt, setDt] = useState("00:00:00");

  useEffect(() => {
    let secTimer = setInterval(() => {
      setDt(timer(startDate));
    }, 1000);

    return () => clearInterval(secTimer);
  }, []);

  return (
    <View>
      <Text style={{ ...styles.timer, ...props.style }}>{dt}</Text>
    </View>
  );
};

export default ActiveWorkoutTimer;

const styles = StyleSheet.create({
  timer: {
    fontFamily: "open-sans",
    paddingLeft: 24,
    fontSize: 16,
    color: Colors.primaryDark,
  },
});
