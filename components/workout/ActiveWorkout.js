import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableNativeFeedback,
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import * as activeWorkoutsActions from "../../store/actions/activeWorkouts";
import ActiveWorkoutTimer from "../workout/ActiveWorkoutTimer";
import Colors from "../../constants/Colors";
import { useSelector, useDispatch } from "react-redux";
import { StackActions, NavigationActions } from "react-navigation";

const ActiveWorkout = (props) => {
  const dispatch = useDispatch();
  const activeWorkout = useSelector(
    (state) => state.activeWorkout.activeWorkout
  );

  let workoutIsActive = useSelector(
    (state) => state.activeWorkout.workoutIsActive
  );

  const onDiscardSelect = (title) => {
    Alert.alert(
      "Are you sure?",
      `Do you really want to discard the current active ${title} workout?`,
      [
        { text: "No", style: "default" },
        {
          text: "Yes",
          style: "destructive",
          onPress: () => {
            dispatch(activeWorkoutsActions.deactivateWorkout());

            props.navigation.dispatch(
              StackActions.reset({
                index: 0,
                key: "Workouts",
                actions: [
                  NavigationActions.navigate({ routeName: "Workouts" }),
                ],
              })
            );
          },
        },
      ]
    );
  };

  const cmp = workoutIsActive ? (
    <TouchableNativeFeedback
      onPress={() => {
        props.navigation.navigate("ActiveWorkout", {
          workoutTitle: activeWorkout.title,
        });
      }}
    >
      <View style={styles.activeWorkout}>
        <View style={styles.info}>
          <View>
            <Text style={styles.titleText}>{activeWorkout.title}</Text>
          </View>
          <View>
            <ActiveWorkoutTimer startDateTime={activeWorkout.startDateTime} />
          </View>
        </View>
        <View style={styles.icon}>
          <TouchableNativeFeedback
            onPress={() => onDiscardSelect(activeWorkout.title)}
            style={styles.optionsIcon}
          >
            <MaterialCommunityIcons
              name={"progress-close"}
              size={28}
              color={Colors.primaryDark}
            />
          </TouchableNativeFeedback>
        </View>
      </View>
    </TouchableNativeFeedback>
  ) : (
    <View></View>
  );

  return cmp;
};

export default ActiveWorkout;

const styles = StyleSheet.create({
  activeWorkout: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: Colors.accentLighter,
    height: 65,
    borderWidth: 1,
    borderColor: Colors.primaryLight,
  },
  info: { alignSelf: "center" },
  titleText: {
    fontFamily: "open-sans",
    paddingLeft: 24,
    fontSize: 20,
    color: Colors.primaryDark,
  },
  icon: {
    marginVertical: 4,
    alignSelf: "center",
    paddingRight: 25,
  },
  optionsIcon: {
    alignSelf: "center",
  },
});
