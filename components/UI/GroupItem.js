import React from "react";
import { StyleSheet, Text, View, TouchableNativeFeedback } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Colors from "../../constants/Colors";

const GroupItem = (props) => {
  return (
    <TouchableNativeFeedback
      onPress={() => {
        props.onItemSelect(props.id);
      }}
    >
      <View style={styles.exercise}>
        <View style={styles.itemText}>
          <Text style={styles.itemTitle}>{props.title}</Text>
          <Text style={styles.itemDescription}>{props.description}</Text>
        </View>
        {props.showOptions ? (
          <View style={styles.options}>
            <TouchableNativeFeedback
              onPress={() => {
                props.onOptionsSelect(props.id);
              }}
              style={styles.optionsIcon}
            >
              <MaterialCommunityIcons
                name={"dots-vertical"}
                size={23}
                color={Colors.primaryDark}
              />
            </TouchableNativeFeedback>
          </View>
        ) : null}
      </View>
    </TouchableNativeFeedback>
  );
};

export default GroupItem;

const styles = StyleSheet.create({
  exercise: {
    fontFamily: "open-sans",
    flexDirection: "row",
    alignItems: "flex-start",
    paddingVertical: 6,
    marginHorizontal: 4,
    color: Colors.primaryDark,
    width: "90%",
  },
  itemText: {
    fontFamily: "open-sans",
    flexDirection: "column",
    alignItems: "flex-start",
    paddingVertical: 6,
    marginHorizontal: 4,
    color: Colors.primaryDark,
    width: "90%",
  },
  itemTitle: {
    fontFamily: "open-sans",
    fontSize: 16,
  },
  itemDescription: {
    fontFamily: "open-sans",
    fontSize: 10,
  },
  options: {
    marginVertical: 4,
    alignSelf: "center",
  },
  optionsIcon: {
    backgroundColor: Colors.accentDark,
    marginLeft: 20,
    alignSelf: "stretch",
  },
});
