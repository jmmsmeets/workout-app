import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Colors from "../../constants/Colors";

const GroupCard = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={styles.text}>
          <Text style={styles.groupTitle}>{props.groupTitle}</Text>
        </View>
        <View style={styles.group}>{props.children}</View>
      </View>
    </View>
  );
};

export default GroupCard;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 16,
    // flexDirection: "column",
    backgroundColor: "#f7f7f7",
  },
  content: {
    //flex: 1,
  },
  text: {
    marginBottom: 5,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  // item: {
  //   padding: 15,
  //   marginVertical: 1,
  //   width: "100%",
  // },
  groupTitle: {
    fontFamily: "open-sans-bold",
    paddingLeft: 24,
    fontSize: 24,
    color: Colors.primaryLight,
  },
  group: {
    elevation: 1,
    marginVertical: 5,
    width: "100%",
    backgroundColor: "#fff",
  },
});
