class CompletedWorkout {
  constructor(id, workoutId, title, dateCompleted, workoutExercises) {
    this.id = id;
    this.workoutId = workoutId;
    this.title = title;
    this.dateCompleted = dateCompleted;
    this.workoutExercises = workoutExercises;
  }
}

export default CompletedWorkout;
