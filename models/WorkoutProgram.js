export class WorkoutProgram {
  constructor(id, title, workoutExercises) {
    this.id = id;
    this.title = title;
    this.workoutExercises = workoutExercises;
  }
}

export class WorkoutExercise {
  constructor(id, workoutId, exerciseId, sets, notes) {
    this.id = id;
    this.workoutId = workoutId;
    this.exerciseId = exerciseId;
    this.sets = sets;
    this.notes = notes;
  }
}

export class Set {
  constructor(id, workoutExerciseId, number, weight, reps) {
    this.id = id;
    this.workoutExerciseId = workoutExerciseId;
    this.number = number;
    this.weight = weight;
    this.reps = reps;
  }
}
