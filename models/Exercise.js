class Exercise {
  constructor(id, title, category, muscleGroups) {
    this.id = id;
    this.title = title;
    this.category = category;
    this.muscleGroups = muscleGroups;
  }
}

export default Exercise;
