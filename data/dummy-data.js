import Exercise from "../models/Exercise";
import { WorkoutProgram, WorkoutExercise, Set } from "../models/WorkoutProgram";
import CompletedWorkout from "../models/CompletedWorkout";

export const CATEGORIES = [
  "Barbell",
  "Dumbbell",
  "Bodyweight",
  "Slamball",
  "Banded",
];

export const MUSCLEGROUPS = [
  "Quadriceps",
  "Glutes",
  "Core",
  "Abs",
  "Triceps",
  "Shoulders",
  "Hamstrings",
  "Calfs",
];

export const EXERCISES = [
  new Exercise("1", "Barbell Squat", "Barbell", [
    "Quadriceps",
    "Glutes",
    "Core",
  ]),
  new Exercise("2", "Barbell Bench press", "Barbell", [
    "Chest",
    "Shoulders",
    "Triceps",
  ]),
  new Exercise("3", "Deadlift", "Barbell", ["Quadriceps", "Glutes", "Core"]),
  new Exercise("4", "DB Clean", "Dumbbell", ["Quadriceps", "Glutes", "Core"]),
  new Exercise("5", "Heels to the heaven", "Bodyweight", ["Abs"]),

  new Exercise("6", "Bulgarian split squat", "Bodyweight", [
    "Quadriceps",
    "Glutes",
    "Core",
  ]),
  new Exercise("7", "Skullcrushers", "Barbell", ["Triceps"]),
  new Exercise("8", "Clean + Jerk", "Barbell", ["Core"]),
  new Exercise("9", "Power Snatch", "Barbell", [
    "Core",
    "Schoulders",
    "Quadriceps",
  ]),
  new Exercise("10", "Jackal Row", "Slamball", ["Quadriceps", "Back", "Core"]),
  new Exercise("11", "Toes to bar", "Bodyweight", ["Abs"]),
  new Exercise("12", "Goodmorning", "Barbell", ["Hamstrings"]),
  new Exercise("13", "Calf raises", "Bodyweight", ["Calfs"]),
];

export const WORKOUTS = [
  new WorkoutProgram("WP40", "Leg day", [
    new WorkoutExercise("WE41", "WP40", "1", [
      new Set("100w", "WE41", "W", "20", "12"),
      new Set("100", "WE41", "1", "50", "10"),
      new Set("101", "WE41", "2", "60", "8"),
      new Set("102", "WE41", "3", "70", "6"),
    ]),
    new WorkoutExercise("WE42", "WP40", "6", [
      new Set("103", "WE42", "1", "20", "12"),
      new Set("104", "WE42", "2", "22.5", "10"),
      new Set("105", "WE42", "3", "25", "8"),
    ]),
    new WorkoutExercise("WE43", "WP40", "12", [
      new Set("112", "WE43", "1", "20", "12"),
      new Set("113", "WE43", "2", "22.5", "10"),
      new Set("114", "WE43", "3", "25", "8"),
    ]),
    new WorkoutExercise("WE44", "WP40", "13", [
      new Set("115", "WE44", "1", "20", "12"),
      new Set("116", "WE44", "2", "22.5", "10"),
      new Set("117", "WE44", "3", "25", "8"),
    ]),
  ]),
  new WorkoutProgram("WP50", "Chest crusher", [
    new WorkoutExercise("WE51", "WP50", "2", [
      new Set("106", "WE51", "1", "40", "12"),
      new Set("107", "WE51", "2", "50", "10"),
      new Set("108", "WE51", "3", "60", "8"),
    ]),
    new WorkoutExercise("WE52", "WP50", "4", [
      new Set("109", "WE52", "1", "20", "12"),
      new Set("110", "WE52", "2", "22.5", "10"),
      new Set("111", "WE52", "3", "25", "8"),
    ]),
  ]),
];

export const COMPLETED_WORKOUTS = [
  new CompletedWorkout(
    "1",
    "WP40",
    "Leg day",
    new Date("2021-01-01T07:00:00.000Z"),
    [
      new WorkoutExercise("WE41", "WP40", "1", [
        new Set("100w", "WE41", "W", "20", "12"),
        new Set("100", "WE41", "1", "50", "10"),
        new Set("101", "WE41", "2", "60", "8"),
        new Set("102", "WE41", "3", "70", "6"),
      ]),
      new WorkoutExercise("WE42", "WP40", "6", [
        new Set("103", "WE42", "1", "20", "12"),
        new Set("104", "WE42", "2", "22.5", "10"),
        new Set("105", "WE42", "3", "25", "8"),
      ]),
      new WorkoutExercise("WE43", "WP40", "12", [
        new Set("112", "WE43", "1", "20", "12"),
        new Set("113", "WE43", "2", "22.5", "10"),
        new Set("114", "WE43", "3", "25", "8"),
      ]),
      new WorkoutExercise("WE44", "WP40", "13", [
        new Set("115", "WE44", "1", "20", "12"),
        new Set("116", "WE44", "2", "22.5", "10"),
        new Set("117", "WE44", "3", "25", "8"),
      ]),
    ]
  ),
  new CompletedWorkout(
    "2",
    "WP50",
    "Chest crusher",
    new Date("2021-01-02T07:00:00.000Z"),
    [
      new WorkoutExercise("WE51", "WP50", "2", [
        new Set("106", "WE51", "1", "40", "12"),
        new Set("107", "WE51", "2", "50", "10"),
        new Set("108", "WE51", "3", "60", "8"),
      ]),
      new WorkoutExercise("WE52", "WP50", "4", [
        new Set("109", "WE52", "1", "20", "12"),
        new Set("110", "WE52", "2", "22.5", "10"),
        new Set("111", "WE52", "3", "25", "8"),
      ]),
    ]
  ),
  new CompletedWorkout(
    "3",
    "WP40",
    "Leg day",
    new Date("2021-02-05T07:00:00.000Z"),
    [
      new WorkoutExercise("WE41", "WP40", "1", [
        new Set("100w", "WE41", "W", "20", "12"),
        new Set("100", "WE41", "1", "50", "10"),
        new Set("101", "WE41", "2", "60", "8"),
        new Set("102", "WE41", "3", "70", "6"),
      ]),
      new WorkoutExercise("WE42", "WP40", "6", [
        new Set("103", "WE42", "1", "20", "12"),
        new Set("104", "WE42", "2", "22.5", "10"),
        new Set("105", "WE42", "3", "25", "8"),
      ]),
      new WorkoutExercise("WE43", "WP40", "12", [
        new Set("112", "WE43", "1", "20", "12"),
        new Set("113", "WE43", "2", "22.5", "10"),
        new Set("114", "WE43", "3", "25", "8"),
      ]),
      new WorkoutExercise("WE44", "WP40", "13", [
        new Set("115", "WE44", "1", "20", "12"),
        new Set("116", "WE44", "2", "22.5", "10"),
        new Set("117", "WE44", "3", "25", "8"),
      ]),
    ]
  ),
  new CompletedWorkout(
    "4",
    "WP50",
    "Chest crusher",
    new Date("2021-03-06T07:00:00.000Z"),
    [
      new WorkoutExercise("WE51", "WP50", "2", [
        new Set("106", "WE51", "1", "40", "12"),
        new Set("107", "WE51", "2", "50", "10"),
        new Set("108", "WE51", "3", "60", "8"),
      ]),
      new WorkoutExercise("WE52", "WP50", "4", [
        new Set("109", "WE52", "1", "20", "12"),
        new Set("110", "WE52", "2", "22.5", "10"),
        new Set("111", "WE52", "3", "25", "8"),
      ]),
    ]
  ),
];
