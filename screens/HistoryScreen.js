import React from "react";
import {
  StyleSheet,
  View,
  FlatList,
} from "react-native";
import { useSelector } from "react-redux";
import ActiveWorkout from "../components/workout/ActiveWorkout";
import GroupCard from "../components/UI/GroupCard";
import GroupItem from "../components/UI/GroupItem";
import Colors from "../constants/Colors";

import moment from "moment";

const HistoryScreen = (props) => {
  let completedWorkouts = useSelector(
    (state) => state.completedWorkouts.completedWorkouts
  );

  const groupCompletedWorkouts = (completedWorkouts) => {
    let groupedWorkouts = completedWorkouts.reduce(
      (accumulator, completedWorkout) => {
        //console.log("r: ", accumulator);

        let groupDate = moment(completedWorkout.dateCompleted).format(
          "MMMM YYYY"
        );
        // if there is no property in accumulator with this date/year, create it
        if (!accumulator[groupDate]) {
          accumulator[groupDate] = {
            groupDate,
            completedWorkouts: [completedWorkout],
          };
        }
        // if there is push current element to workout array for that date/year
        else {
          accumulator[groupDate].completedWorkouts.push(completedWorkout);
        }
        return accumulator;
      },
      {}
    );

    // since groupedWorkouts at this point is an object, to get array of values use Object.values method
    let result = Object.values(groupedWorkouts);

    return result;
  };

  completedWorkouts.sort(function (a, b) {
    var firstDate = a.dateCompleted;
    var secondDate = b.dateCompleted;
    if (firstDate > secondDate) {
      return -1;
    }
    if (firstDate < secondDate) {
      return 1;
    }
    return 0;
  });

  const selectWorkoutHandler = (id, title) => {
    props.navigation.navigate("HistoryWorkoutDetails", {
      workoutId: id,
      title: title,
    });
  };

  let result = groupCompletedWorkouts(completedWorkouts);

  return (
    <View style={styles.screen}>
      <View style={styles.flatList}>
        <FlatList
          data={result}
          keyExtractor={(item) => item.groupDate}
          renderItem={(itemData) => {
            const group = itemData.item;
            return (
              <GroupCard groupTitle={group.groupDate}>
                {group.completedWorkouts.map((completedWorkout) => {
                  return (
                    <GroupItem
                      id={completedWorkout.id}
                      title={completedWorkout.title}
                      description={moment(
                        completedWorkout.dateCompleted
                      ).format("DD-MMMM-YYYY HH:mm")}
                      onItemSelect={() => {
                        selectWorkoutHandler(
                          completedWorkout.id,
                          completedWorkout.title
                        );
                      }}
                      showOptions={false}
                      key={completedWorkout.id}
                    />
                  );
                })}
              </GroupCard>
            );
          }}
          extraData={props.navigation}
        ></FlatList>
      </View>
      <ActiveWorkout navigation={props.navigation} />
    </View>
  );
};

export default HistoryScreen;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "space-between",
  },
  list: {
    flex: 1,
    justifyContent: "flex-start",
  },
  exercise: {
    fontFamily: "open-sans",
    flexDirection: "row",
    alignItems: "flex-start",
    paddingVertical: 6,
    marginHorizontal: 4,
    color: Colors.primaryDark,
    width: "90%",
  },
  itemText: {
    fontFamily: "open-sans",
    flexDirection: "column",
    alignItems: "flex-start",
    paddingVertical: 6,
    marginHorizontal: 4,
    color: Colors.primaryDark,
    width: "90%",
  },
  itemTitle: {
    fontFamily: "open-sans",
    fontSize: 16,
  },
  itemDescription: {
    fontFamily: "open-sans",
    fontSize: 10,
  },
  options: {
    marginVertical: 4,
    alignSelf: "center",
  },
  optionsIcon: {
    backgroundColor: Colors.accentDark,
    marginLeft: 20,
    alignSelf: "stretch",
  },
});
