import React, { useState, useEffect, useCallback } from "react";
import { View, Button, StyleSheet, FlatList, Text } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { WORKOUTS } from "../data/dummy-data";
import Colors from "../constants/Colors";
import WorkoutExerciseCard from "../components/workout/WorkoutExerciseCard";
import * as activeWorkoutsActions from "../store/actions/activeWorkouts";
import * as completedWorkoutsActions from "../store/actions/completedWorkouts";
import { StackActions, NavigationActions } from "react-navigation";
import ActiveWorkoutTimer from "../components/workout/ActiveWorkoutTimer";

const ActiveWorkoutScreen = (props) => {
  let activeWorkout = useSelector((state) => state.activeWorkout.activeWorkout);
  let workoutIsActive = useSelector(
    (state) => state.activeWorkout.workoutIsActive
  );
  const dispatch = useDispatch();

  let workout = null;
  if (workoutIsActive) {
    workout = WORKOUTS.find((w) => w.id === activeWorkout.id);
  }

  const saveWorkoutHandler = () => {
    dispatch(completedWorkoutsActions.saveActiveWorkout(activeWorkout));
    dispatch(activeWorkoutsActions.deactivateWorkout());

    props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "Workouts" })],
      })
    );
    props.navigation.navigate("History");
  };

  let exercises = useSelector((state) => state.exercises.exercises);
  return activeWorkout ? (
    <View style={styles.screen}>
      <ActiveWorkoutTimer
        startDateTime={activeWorkout.startDateTime}
        style={{
          fontSize: 24,
          color: Colors.primaryDark,
          backgroundColor: Colors.accentLighter,
          paddingVertical: 5,
          justifyContent: "space-between",
        }}
      />
      <FlatList
        data={workout.workoutExercises}
        keyExtractor={(item) => item.id}
        renderItem={(itemData) => {
          const exercise = exercises.find(
            (e) => e.id === itemData.item.exerciseId
          );
          return (
            <WorkoutExerciseCard
              key={itemData.id}
              isActiveWorkout={true}
              title={exercise.title}
              sets={itemData.item.sets}
              weId={itemData.item.id}
            ></WorkoutExerciseCard>
          );
        }}
        extraData={props.navigation}
      ></FlatList>
      <Button
        title={"Finish"}
        color={Colors.primaryLight}
        onPress={() => {
          saveWorkoutHandler();
        }}
      ></Button>
    </View>
  ) : (
    <View></View>
  );
};

ActiveWorkoutScreen.navigationOptions = (navigationData) => {
  //you cant use UseSelector() here, hooks can only be used in other hooks, of functional components
  const workoutTitle = navigationData.navigation.getParam("workoutTitle");
  return {
    headerTitle: workoutTitle,
  };
};

export default ActiveWorkoutScreen;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "space-between",
  },
  container: {
    paddingVertical: 16,
    // flexDirection: "column",
    backgroundColor: "#f7f7f7",
  },
  content: {
    //flex: 1,
  },
  setColumnHeaders: {
    flexDirection: "row",
  },
  columnHeaders: {
    fontFamily: "open-sans-bold",
    fontSize: 24,
    paddingHorizontal: 10,
  },
  text: {
    marginBottom: 5,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  groupTitle: {
    fontFamily: "open-sans-bold",
    paddingLeft: 24,
    fontSize: 24,
    color: Colors.primaryLight,
  },
  group: {
    elevation: 1,
    marginVertical: 5,
    width: "100%",
    backgroundColor: "#fff",
  },
  set: {
    flexDirection: "row",
  },
});
