import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableNativeFeedback } from "react-native";
import { useSelector } from "react-redux";
import GroupCard from "../components/UI/GroupCard";
import GroupItem from "../components/UI/GroupItem";
import { WORKOUTS } from "../data/dummy-data";
import ActiveWorkout from "../components/workout/ActiveWorkout";

const WorkoutsScreen = (props) => {
  let exercises = useSelector((state) => state.exercises.exercises);
  const [Exercises, setExercises] = useState(exercises);

  const selectWorkoutEditHandler = (id) => {
    props.navigation.navigate("WorkoutEdit", {
      workoutId: id,
    });
  };

  const selectWorkoutHandler = (id, title) => {
    props.navigation.navigate("WorkoutStart", {
      workoutId: id,
      workoutTitle: title,
    });
  };

  return (
    <React.Fragment>
      <View style={styles.screen}>
        <GroupCard groupTitle="Default workouts">
          {WORKOUTS.map((workout) => {
            let description = "";

            workout.workoutExercises.forEach((e) => {
              const exercise = Exercises.find((ex) => ex.id === e.exerciseId);
              //console.log(exercise);
              description =
                description + e.sets.length + "x " + exercise.title + ",    ";
            });

            return (
              <GroupItem
                key={workout.id}
                id={workout.id}
                title={workout.title}
                description={description}
                onOptionsSelect={selectWorkoutEditHandler}
                onItemSelect={() => {
                  selectWorkoutHandler(workout.id, workout.title);
                }}
              />
            );
          })}
        </GroupCard>
      </View>
      <ActiveWorkout navigation={props.navigation} />
    </React.Fragment>
  );
};

export default WorkoutsScreen;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "space-between",
  },
});
