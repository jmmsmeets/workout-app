import React from "react";
import { useSelector } from "react-redux";
import { StyleSheet, Text, View } from "react-native";
import Colors from "../constants/Colors";

import moment from "moment";

const HistoryWorkoutDetailsScreen = (props) => {
  const workoutId = props.navigation.getParam("workoutId");

  const completedWorkout = useSelector((state) =>
    state.completedWorkouts.completedWorkouts.find((w) => w.id === workoutId)
  );

  let exercises = useSelector((state) => state.exercises.exercises);
  return (
    <View>
      <View>
        <Text style={styles.dateCompleted}>
          Completed:{" "}
          {moment(completedWorkout.dateCompleted).format("DD-MMMM-YYYY HH:mm")}
        </Text>
      </View>

      {completedWorkout.workoutExercises.map((e) => {
        const exercise = exercises.find(
          (exercise) => exercise.id === e.exerciseId
        );
        return (
          <View style={styles.container} key={e.id}>
            <View style={styles.group}>
              <View style={styles.text}>
                <Text style={styles.title}>{exercise.title}</Text>
              </View>
              {e.sets.map((s) => {
                return (
                  <View key={s.id}>
                    <Text style={styles.defaultText}>
                      {s.weight}kg - {s.reps}x
                    </Text>
                  </View>
                );
              })}
            </View>
          </View>
        );
      })}
    </View>
  );
};

HistoryWorkoutDetailsScreen.navigationOptions = (navigationData) => {
  //you cant use UseSelector() here, hooks can only be used in other hooks, of functional components
  const workoutTitle = navigationData.navigation.getParam("title");
  return {
    headerTitle: "History of " + workoutTitle,
  };
};

export default HistoryWorkoutDetailsScreen;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 6,
    // flexDirection: "column",
    backgroundColor: "#f7f7f7",
    paddingLeft: 24,
  },
  dateCompleted: {
    paddingLeft: 24,
    padding: 12,
    fontFamily: "open-sans",
    fontSize: 14,
    color: "black",
  },
  text: {
    marginBottom: 2,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  groupTitle: {
    fontFamily: "open-sans-bold",
    paddingLeft: 24,
    paddingTop: 16,
    fontSize: 24,
    color: Colors.primaryLight,
  },
  title: {
    fontFamily: "open-sans-bold",
    fontSize: 14,
    color: "black",
  },
  defaultText: {
    fontFamily: "open-sans",
    fontSize: 14,
    color: "black",
  },
  group: {
    elevation: 1,
    marginVertical: 2,
    width: "100%",
    backgroundColor: "#fff",
  },
});
