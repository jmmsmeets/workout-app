import React from "react";
import { Button, StyleSheet, Alert, View } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import GroupCard from "../components/UI/GroupCard";
import GroupItem from "../components/UI/GroupItem";
import { WORKOUTS } from "../data/dummy-data";
import Colors from "../constants/Colors";
import * as activeWorkoutsActions from "../store/actions/activeWorkouts";
import { StackActions, NavigationActions } from "react-navigation";

const WorkoutStartScreen = (props) => {
  const workoutId = props.navigation.getParam("workoutId");
  const workout = WORKOUTS.find((w) => w.id === workoutId);
  let exercises = useSelector((state) => state.exercises.exercises);

  const dispatch = useDispatch();
  let workoutAllreadyActive = useSelector(
    (state) => state.activeWorkout.workoutIsActive
  );

  const selectExerciseHandler = (id, title) => {
    props.navigation.navigate("ExerciseDetails", {
      exerciseId: id,
      title: title,
    });
  };

  const selectExerciseOptionsHandler = (id, title) => {
    props.navigation.navigate("ExerciseEdit", {
      exerciseId: id,
      title: title,
    });
  };

  const startWorkoutHandler = () => {
    if (!workoutAllreadyActive) {
      dispatch(activeWorkoutsActions.activateWorkout(workout));
      props.navigation.navigate("ActiveWorkout", {
        workoutTitle: workout.title,
      });
    } else {
      Alert.alert(
        "Another active workout",
        `Do you want to discard the current active workout, and start ${workout.title}?`,
        [
          { text: "No", style: "default" },
          {
            text: "Yes",
            style: "destructive",
            onPress: () => {
              dispatch(activeWorkoutsActions.deactivateWorkout());

              props.navigation.dispatch(
                StackActions.reset({
                  index: 0,
                  key: "Workouts",
                  actions: [
                    NavigationActions.navigate({ routeName: "Workouts" }),
                  ],
                })
              );

              dispatch(activeWorkoutsActions.activateWorkout(workout));
              props.navigation.navigate("ActiveWorkout", {
                workoutTitle: workout.title,
              });
            },
          },
        ]
      );
    }
  };

  return (
    <View style={styles.screen}>
      <GroupCard>
        {workout.workoutExercises.map((e) => {
          const exercise = exercises.find((ex) => ex.id === e.exerciseId);
          return (
            <GroupItem
              id={exercise.id}
              title={"3x " + exercise.title}
              description={exercise.category}
              onItemSelect={() =>
                selectExerciseHandler(exercise.id, exercise.title)
              }
              onOptionsSelect={() =>
                selectExerciseOptionsHandler(exercise.id, exercise.title)
              }
              key={exercise.id}
            />
          );
        })}
      </GroupCard>
      <View style={{ paddingBottom: 25 }}>
        <Button
          title={"START WORKOUT"}
          color={Colors.primaryLight}
          onPress={() => {
            startWorkoutHandler();
          }}
        ></Button>
      </View>
    </View>
  );
};

WorkoutStartScreen.navigationOptions = (navigationData) => {
  //you cant use UseSelector() here, hooks can only be used in other hooks, of functional components
  const workoutTitle = navigationData.navigation.getParam("workoutTitle");
  return {
    headerTitle: "Start " + workoutTitle,
  };
};

export default WorkoutStartScreen;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "space-between",
  },
  itemTitle: {
    fontFamily: "open-sans",
    fontSize: 16,
  },
});
