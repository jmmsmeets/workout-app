import React from "react";
import { StyleSheet, FlatList } from "react-native";
import { useSelector } from "react-redux";
import { WORKOUTS } from "../data/dummy-data";
import Colors from "../constants/Colors";
import WorkoutExerciseCard from "../components/workout/WorkoutExerciseCard";

const WorkoutEditScreen = (props) => {
  const workoutId = props.navigation.getParam("workoutId");
  const workout = WORKOUTS.find((w) => w.id === workoutId);
  let exercises = useSelector((state) => state.exercises.exercises);
  return (
    <FlatList
      data={workout.workoutExercises}
      keyExtractor={(item) => item.id}
      renderItem={(itemData) => {
        const exercise = exercises.find(
          (e) => e.id === itemData.item.exerciseId
        );
        return (
          <WorkoutExerciseCard
            isActiveWorkout={false}
            title={exercise.title}
            sets={itemData.item.sets}
          ></WorkoutExerciseCard>
        );
      }}
      extraData={props.navigation}
    ></FlatList>
  );
};

export default WorkoutEditScreen;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 16,
    // flexDirection: "column",
    backgroundColor: "#f7f7f7",
  },
  content: {
    //flex: 1,
  },
  setColumnHeaders: {
    flexDirection: "row",
  },
  columnHeaders: {
    fontFamily: "open-sans-bold",
    fontSize: 24,
    paddingHorizontal: 10,
  },
  text: {
    marginBottom: 5,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  groupTitle: {
    fontFamily: "open-sans-bold",
    paddingLeft: 24,
    fontSize: 24,
    color: Colors.primaryLight,
  },
  group: {
    elevation: 1,
    marginVertical: 5,
    width: "100%",
    backgroundColor: "#fff",
  },
  set: {
    flexDirection: "row",
  },
});
