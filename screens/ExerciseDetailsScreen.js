import React from "react";
import { StyleSheet, Text, View, Dimensions, StatusBar } from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import Colors from "../constants/Colors";

const HistoryRoute = () => (
  <View style={[styles.scene, { backgroundColor: "white" }]} />
);

const RecordsRoute = () => (
  <View style={[styles.scene, { backgroundColor: "white" }]} />
);

const ChartRoute = () => (
  <View style={[styles.scene, { backgroundColor: "white" }]}>
    <Text>Chart</Text>
  </View>
);

const initialLayout = { width: Dimensions.get("window").width };

const ExerciseDetailsScreen = (props) => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: "history", title: "History" },
    { key: "records", title: "Records" },
    { key: "chart", title: "Chart" },
  ]);

  const renderScene = SceneMap({
    history: HistoryRoute,
    records: RecordsRoute,
    chart: ChartRoute,
  });

  return (
    <TabView
      navigationState={{ index, routes }}
      renderTabBar={(props) => (
        <TabBar
          {...props}
          indicatorStyle={{ backgroundColor: Colors.accentLight }}
          renderLabel={({ route, color }) => (
            <Text style={{ color: Colors.primaryDark, margin: 8 }}>
              {route.title}
            </Text>
          )}
          style={{ backgroundColor: "white" }}
        />
      )}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      style={styles.container}
    />
  );
};

ExerciseDetailsScreen.navigationOptions = (navigationData) => {
  //you cant use UseSelector() here, hooks can only be used in other hooks, of functional components
  const title = navigationData.navigation.getParam("title");
  return {
    headerTitle: title + " details",
  };
};

export default ExerciseDetailsScreen;

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});
