import React, { useState, useEffect } from "react";
import { StyleSheet, View, SafeAreaView, FlatList, Text } from "react-native";
import { useSelector } from "react-redux";
import { CATEGORIES, MUSCLEGROUPS } from "../data/dummy-data";
import HeaderButton from "../components/UI/HeaderButton";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { Entypo } from "@expo/vector-icons";
import DropDownPicker from "react-native-dropdown-picker";
import GroupCard from "../components/UI/GroupCard";
import GroupItem from "../components/UI/GroupItem";
// import { withNavigationFocus } from "react-navigation";
import ActiveWorkout from "../components/workout/ActiveWorkout";

const ExercisesScreen = (props) => {
  let exercises = useSelector((state) => state.exercises.exercises);

  // useEffect(() => {
  //   if (props.isFocused) {
  //     categorySelectHandler("any");
  //   }
  // });

  //console.log(exercises);

  const [Exercises, setExercises] = useState(exercises);
  const [Categories, setCategories] = useState(CATEGORIES);
  const [MuscleGroups, setMuscleGroups] = useState(MUSCLEGROUPS);

  const groupExercises = (Exercises) => {
    let groupedExercises = Exercises.reduce((accumulator, exercise) => {
      //console.log("r: ", accumulator);
      // get first letter of name of current element
      let groupLetter = exercise.title[0];
      // if there is no property in accumulator with this letter create it
      if (!accumulator[groupLetter]) {
        accumulator[groupLetter] = { groupLetter, exercises: [exercise] };
      }
      // if there is push current element to exercises array for that letter
      else {
        accumulator[groupLetter].exercises.push(exercise);
      }
      return accumulator;
    }, {});

    // since groupedExercises at this point is an object, to get array of values use Object.values method
    let result = Object.values(groupedExercises);

    result.sort(function (a, b) {
      var firstLetter = a.groupLetter.toUpperCase();
      var secondletter = b.groupLetter.toUpperCase();
      if (firstLetter < secondletter) {
        return -1;
      }
      if (firstLetter > secondletter) {
        return 1;
      }
      return 0;
    });

    return result;
  };

  const categorySelectHandler = (value) => {
    if (value !== "any") {
      const filteredExercises = exercises.filter(
        (e) => e.category.toLowerCase() === value.toLowerCase()
      );
      setExercises(filteredExercises);
    } else {
      setExercises(exercises);
    }
  };

  const selectExerciseHandler = (id, title) => {
    props.navigation.navigate("ExerciseDetails", {
      exerciseId: id,
      title: title,
    });
  };

  const selectExerciseOptionsHandler = (id, title) => {
    props.navigation.navigate("ExerciseEdit", {
      exerciseId: id,
      title: title,
    });
  };

  let result = groupExercises(Exercises);

  let categoryItems = () => {
    let categoryItems = [{ value: "any", label: "- Any category -" }];

    let categories = Categories.map((item) => ({
      value: item.toLowerCase(),
      label: item,
    }));

    categories.forEach((c) => categoryItems.push(c));
    return categoryItems;
  };

  let muscleGroupItems = MuscleGroups.map((item) => ({
    value: item.toLowerCase(),
    label: item,
  }));

  return (
    <View style={styles.screen}>
      <View style={styles.dropdownRow}>
        <DropDownPicker
          items={categoryItems()}
          placeholder="- Any category -"
          // defaultValue={{ value: "def", label: "Def" }}
          containerStyle={{ height: 50, width: "50%" }}
          style={{ backgroundColor: "#fafafa" }}
          itemStyle={{
            justifyContent: "flex-start",
          }}
          dropDownStyle={{ backgroundColor: "#fafafa" }}
          onChangeItem={(item) => {
            categorySelectHandler(item.value);
          }}
        />
        <DropDownPicker
          items={muscleGroupItems}
          placeholder="- Any muscle group -"
          containerStyle={{ height: 50, width: "50%" }}
          style={{ backgroundColor: "#fafafa" }}
          itemStyle={{
            justifyContent: "flex-start",
          }}
          dropDownStyle={{ backgroundColor: "#fafafa" }}
          onChangeItem={(item) => {
            //console.log(`${item.value} selected`);
          }}
        />
      </View>
      <View style={styles.flatList}>
        <FlatList
          data={result}
          keyExtractor={(item) => item.groupLetter}
          renderItem={(itemData) => {
            const group = itemData.item;
            return (
              <GroupCard groupTitle={group.groupLetter}>
                {group.exercises.map((exercise) => {
                  return (
                    <GroupItem
                      id={exercise.id}
                      title={exercise.title}
                      description={exercise.category}
                      onItemSelect={() =>
                        selectExerciseHandler(exercise.id, exercise.title)
                      }
                      onOptionsSelect={() =>
                        selectExerciseOptionsHandler(
                          exercise.id,
                          exercise.title
                        )
                      }
                      key={exercise.id}
                    />
                  );
                })}
              </GroupCard>
            );
          }}
          extraData={props.navigation}
        ></FlatList>
      </View>
      <ActiveWorkout navigation={props.navigation} />
    </View>
  );
};

ExercisesScreen.navigationOptions = (navData) => {
  return {
    headerRight: () => (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="AddExercise"
          iconName={"add"}
          IconComponent={Entypo}
          onPress={() => {
            navData.navigation.navigate("ExerciseEdit");
          }}
        />
      </HeaderButtons>
    ),
  };
};

export default ExercisesScreen; //withNavigationFocus(ExercisesScreen);

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "space-between",
  },
  dropdownRow: {
    flexDirection: "row",
  },
  flatList: {
    flex: 1,
  },
});
