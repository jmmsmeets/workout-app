import React, { useState, useEffect, useCallback, useReducer } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Alert,
} from "react-native";
import { Picker } from "@react-native-picker/picker";
import Checkbox from "@react-native-form/checkbox";
import { CATEGORIES, MUSCLEGROUPS } from "../data/dummy-data";
import Colors from "../constants/Colors";

import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/UI/HeaderButton";

import { useSelector, useDispatch } from "react-redux";
import * as exercisesActions from "../store/actions/exercises";

const formReducer = (state, action) => {
  if (action.type === "FORM_INPUT_UPDATE") {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value,
    };

    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid,
    };

    let updatedFormisValid = true;
    for (const key in updatedValidities) {
      updatedFormisValid = updatedFormisValid && updatedValidities[key];
    }
    return {
      formIsValid: updatedFormisValid,
      inputValues: updatedValues,
      inputValidities: updatedValidities,
    };
  }
  if (action.type === "FORM_INPUT_MUSCLEGROUPS_UPDATE") {
    let musclegroups = [...state.inputValues.muscleGroups];

    if (action.value) {
      musclegroups.push(action.label);
    } else {
      musclegroups.splice(musclegroups.indexOf(action.label), 1);
    }

    const updatedValues = {
      ...state.inputValues,
      muscleGroups: musclegroups,
    };

    return {
      ...state,
      inputValues: updatedValues,
    };
  }
  return state;
};

const ExerciseEditScreen = (props) => {
  const exerciseId = props.navigation.getParam("exerciseId");

  const editedExercise = useSelector((state) =>
    state.exercises.exercises.find((exercise) => exercise.id === exerciseId)
  );

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      title: editedExercise ? editedExercise.title : "",
      category: editedExercise ? editedExercise.category : "",
      muscleGroups: editedExercise ? editedExercise.muscleGroups : [],
    },
    inputValidities: {
      title: editedExercise ? true : false,
    },
    formIsValid: editedExercise ? true : false,
  });

  const muscleGroupChangeHandler = (value) => {
    dispatchFormState({
      type: "FORM_INPUT_MUSCLEGROUPS_UPDATE",
      value: value.checked,
      isValid: true,
      input: "muscleGroups",
      label: value.label,
    });
  };

  const categoryChangeHandler = (category) => {
    dispatchFormState({
      type: "FORM_INPUT_UPDATE",
      value: category,
      isValid: true,
      input: "category",
    });
  };

  const titleChangeHandler = (title) => {
    let isValid = false;
    if (title.trim().length > 0) {
      isValid = true;
    }

    dispatchFormState({
      type: "FORM_INPUT_UPDATE",
      value: title,
      isValid: isValid,
      input: "title",
    });
  };

  const dispatch = useDispatch();

  const submitHandler = useCallback(() => {
    if (!formState.formIsValid) {
      Alert.alert("Wrong input", "check errors", [{ text: "Ok" }]);
      return;
    }

    //console.log("submitHandler: ", formState.inputValues.muscleGroups);

    if (editedExercise) {
      dispatch(
        exercisesActions.updateExercise(
          exerciseId,
          formState.inputValues.title,
          formState.inputValues.category,
          formState.inputValues.muscleGroups
        )
      );
    } else {
      dispatch(
        exercisesActions.createExercise(
          formState.inputValues.title,
          formState.inputValues.category,
          formState.inputValues.muscleGroups
        )
      );
    }
    props.navigation.goBack();
  }, [dispatch, exerciseId, formState]);

  useEffect(() => {
    props.navigation.setParams({ submit: submitHandler });
  }, [submitHandler]);

  return (
    <ScrollView>
      <View style={styles.form}>
        <View style={styles.formControl}>
          <Text style={styles.label}>Title</Text>
          <TextInput
            style={styles.input}
            value={formState.inputValues.title}
            onChangeText={titleChangeHandler}
            keyboardType="default"
            autoCapitalize={"sentences"}
          />
          {!formState.inputValidities.titleIsValid && (
            <Text>Please enter valid text</Text>
          )}
        </View>
        <View style={styles.formControl}>
          <Text style={styles.label}>Category</Text>
          <Picker
            selectedValue={formState.inputValues.category}
            // style={{ height: 50, width: 150 }}
            onValueChange={(itemValue, itemIndex) =>
              categoryChangeHandler(itemValue)
            }
          >
            {CATEGORIES.map((category) => (
              <Picker.Item key={category} label={category} value={category} />
            ))}
          </Picker>
        </View>

        <View style={styles.formControl}>
          <Text style={styles.label}>Muscle groups</Text>
          {MUSCLEGROUPS.map((muscleGroup) => {
            let checked = formState.inputValues.muscleGroups.includes(
              muscleGroup
            );
            return (
              <Checkbox
                key={muscleGroup}
                label={muscleGroup}
                positionLabel="right"
                marginTop={0}
                nativeComponent={false}
                color={Colors.primaryLight}
                disabled={false}
                checked={checked}
                labelStyle={{}}
                containerStyle={{}}
                checkboxStyle={{}}
                switchStyle={{}}
                onValueChange={muscleGroupChangeHandler}
              />
            );
          })}
        </View>
      </View>
    </ScrollView>
  );
};

ExerciseEditScreen.navigationOptions = (navigationData) => {
  const submitHandler = navigationData.navigation.getParam("submit");
  const title = navigationData.navigation.getParam("title");
  return {
    headerTitle: navigationData.navigation.getParam("exerciseId")
      ? "Edit " + title
      : "Add exercise",
    headerRight: () => (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item title="Save" iconName={"md-checkmark"} onPress={submitHandler} />
      </HeaderButtons>
    ),
  };
};

export default ExerciseEditScreen;

const styles = StyleSheet.create({
  form: {
    margin: 20,
  },
  formControl: {
    width: "100%",
    paddingVertical: 10,
  },
  label: {
    fontFamily: "open-sans-bold",
    marginVertical: 8,
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
  },
});
