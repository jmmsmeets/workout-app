export default {
  primaryDark: "#264653",
  primaryLight: "#2a9d8f",
  accentLighter: "#f4db9a",
  accentLight: "#e9c46a",
  accent: "#f4a261",
  accentDark: "#e76f51",
};
