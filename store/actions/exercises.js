export const CREATE_EXERCISE = "CREATE_EXERCISE";
export const UPDATE_EXERCISE = "UPDATE_EXERCISE";
export const DELETE_EXERCISE = "DELETE_EXERCISE";

export const createExercise = (title, category, muscleGroups) => {
  return {
    type: CREATE_EXERCISE,
    exerciseData: {
      title,
      category,
      muscleGroups,
    },
  };
};

export const updateExercise = (id, title, category, muscleGroups) => {
  return {
    type: UPDATE_EXERCISE,
    exerciseId: id,
    exerciseData: {
      title,
      category,
      muscleGroups,
    },
  };
};

export const deleteExercise = (id) => {
  return { type: DELETE_EXERCISE, exerciseId: id };
};
