export const SAVE_ACTIVE_WORKOUT = "SAVE_ACTIVE_WORKOUT";

export const saveActiveWorkout = (workout) => {
  return {
    type: SAVE_ACTIVE_WORKOUT,
    workoutData: {
      workout,
    },
  };
};
