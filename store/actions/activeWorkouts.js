export const ACTIVATE_WORKOUT = "ACTIVATE_WORKOUT";
export const DEACTIVATE_WORKOUT = "DEACTIVATE_WORKOUT";
export const SAVE_SET = "SAVE_SET";
export const REMOVE_SET = "REMOVE_SET";

export const activateWorkout = (workout) => {
  return {
    type: ACTIVATE_WORKOUT,
    workoutData: {
      workout,
    },
  };
};

export const deactivateWorkout = () => {
  return {
    type: DEACTIVATE_WORKOUT,
  };
};

export const saveSet = (setId, workoutExerciseId, number, weight, reps) => {
  return {
    type: SAVE_SET,
    setData: {
      setId: setId,
      workoutExerciseId: workoutExerciseId,
      number: number,
      weight: weight,
      reps: reps,
    },
  };
};

export const removeSet = (setId, workoutExerciseId) => {
  return {
    type: REMOVE_SET,
    setData: {
      setId: setId,
      workoutExerciseId: workoutExerciseId,
    },
  };
};
