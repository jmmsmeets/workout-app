import { COMPLETED_WORKOUTS } from "../../data/dummy-data";
import CompletedWorkout from "../../models/CompletedWorkout";
import { SAVE_ACTIVE_WORKOUT } from "../actions/completedWorkouts";

const initialState = {
  completedWorkouts: COMPLETED_WORKOUTS,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SAVE_ACTIVE_WORKOUT:
      const workout = action.workoutData.workout;
      // console.log(
      //   "[completedWorkouts reducer - SAVE ACTIVE WORKOUT] - workout: ",
      //   workout
      // );

      workout.workoutExercises = workout.workoutExercises.filter(
        (e) => e.sets.length > 0
      ); // filter out exercises with no filled out sets

      const completedWorkout = new CompletedWorkout(
        new Date().toString(),
        workout.id,
        workout.title,
        new Date(),
        workout.workoutExercises
      );
      return {
        ...state,
        completedWorkouts: state.completedWorkouts.concat(completedWorkout),
      };
  }
  return state;
};
