import { WORKOUTS } from "../../data/dummy-data";

const initialState = {
  workoutPrograms: WORKOUTS,
};

export default (state = initialState, action) => {
  return state;
};
