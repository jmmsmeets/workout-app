import { EXERCISES } from "../../data/dummy-data";
import Exercise from "../../models/Exercise";
import {
  CREATE_EXERCISE,
  UPDATE_EXERCISE,
  DELETE_EXERCISE,
} from "../actions/exercises";

const initialState = {
  exercises: EXERCISES,
};

export default (state = initialState, action) => {
  //console.log(state);
  switch (action.type) {
    case CREATE_EXERCISE:
      const newExercise = new Exercise(
        new Date().toString(),
        action.exerciseData.title,
        action.exerciseData.category,
        action.exerciseData.muscleGroups
      );
      return { ...state, exercises: state.exercises.concat(newExercise) };
    case UPDATE_EXERCISE:
      const exerciseIndex = state.exercises.findIndex(
        (e) => e.id === action.exerciseId
      );
      const updatedExercise = new Exercise(
        action.exerciseId,
        action.exerciseData.title,
        action.exerciseData.category,
        action.exerciseData.muscleGroups
      );

      const updatedExercises = [...state.exercises];
      updatedExercises[exerciseIndex] = updatedExercise;
      return { ...state, exercises: updatedExercises };
    case DELETE_EXERCISE:
      return state;
    default:
      return state;
  }
};
