import { Set } from "../../models/WorkoutProgram";
import {
  ACTIVATE_WORKOUT,
  DEACTIVATE_WORKOUT,
  SAVE_SET,
  REMOVE_SET,
} from "../actions/activeWorkouts";

const initialState = {
  workoutIsActive: false,
  activeWorkout: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ACTIVATE_WORKOUT:
      console.log("activating, current is: ", state.workoutIsActive);
      const workout = {
        ...action.workoutData.workout,
      };
      workout.startDateTime = new Date();
      workout.workoutExercises = workout.workoutExercises.map((e) => {
        var exercise = { ...e };
        exercise.sets = [];
        return exercise;
      });
      return { ...state, workoutIsActive: true, activeWorkout: workout };
    case DEACTIVATE_WORKOUT:
      console.log("deactivating, current is: ", state.workoutIsActive);
      return { ...state, workoutIsActive: false, activeWorkout: null };

    case SAVE_SET:
      const setData = action.setData;

      let activeWorkout = { ...state.activeWorkout };
      const set = new Set(
        setData.setId,
        setData.workoutExerciseId,
        setData.number,
        setData.weight,
        setData.reps
      );

      const workoutExerciseIndex = activeWorkout.workoutExercises.findIndex(
        (we) => we.id === setData.workoutExerciseId
      );

      activeWorkout.workoutExercises[workoutExerciseIndex].sets.push(set);
      return { ...state, activeWorkout: activeWorkout };
    case REMOVE_SET:
      const activeWorkoutCopy = { ...state.activeWorkout };

      const wExerciseIndex = activeWorkoutCopy.workoutExercises.findIndex(
        (we) => we.id === action.setData.workoutExerciseId
      );

      let wExercise = activeWorkoutCopy.workoutExercises[wExerciseIndex];

      wExercise.sets = wExercise.sets.filter(
        (s) => s.id !== action.setData.setId
      );

      return { ...state, activeWorkout: activeWorkoutCopy };
  }
  return state;
};
